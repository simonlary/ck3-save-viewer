const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
	mode: "development",
	entry: path.join(__dirname, "src", "index.tsx"),
	target: "web",
	resolve: {
		extensions: [".ts", ".tsx", ".js"],
		alias: {
			["@"]: path.join(__dirname, "src")
		}
	},
	module: {
		rules: [
			{
				test: /\.tsx?$/,
				include: path.join(__dirname, "src"),
				use: "ts-loader"
			},
			{
				test: /\.css$/,
				use: [MiniCssExtractPlugin.loader, "css-loader"],
			}
		]
	},
	output: {
		filename: "index.js",
		path: path.join(__dirname, "out")
	},
	devServer: {
		contentBase: path.join(__dirname, "out"),
		hot: true
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: path.join(__dirname, "src", "index.html")
		}),
		new MiniCssExtractPlugin({
			filename: "index.css"
		})
	]
}
