module.exports = {
	printWidth: 120,
	endOfLine: "lf",
	useTabs: true,
}
