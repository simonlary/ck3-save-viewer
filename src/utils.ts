export function convertToReadable(camelCase: string): string {
	return (
		camelCase
			.match(/[A-Za-z][a-z]*/g)
			?.map((w) => w.charAt(0).toUpperCase() + w.substring(1))
			.join(" ") ?? ""
	);
}
