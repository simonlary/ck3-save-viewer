import { MetaData } from "@simonlary/ck3-save-parser/dist/save";
import React from "react";
import { convertToReadable } from "../../utils";
import styles from "./MetaDataView.module.css";

interface MetadataViewProps {
	metadata: MetaData;
}

export function MetadataView(props: MetadataViewProps): JSX.Element {
	return (
		<div className={styles.container}>
			<div>
				<h1>General</h1>
				<table>
					<tbody>
						<tr>
							<th>Number of players</th>
							<td>{props.metadata.meta_number_of_players}</td>
						</tr>
						<tr>
							<th>Character Name</th>
							<td>{props.metadata.meta_player_name}</td>
						</tr>
						<tr>
							<th>Date</th>
							<td>{props.metadata.meta_date}</td>
						</tr>
						<tr>
							<th>Real Date</th>
							<td>{props.metadata.meta_real_date}</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div>
				<h1>Main Character&apos;s Genes</h1>
				<table>
					<tbody>
						{Object.entries(props.metadata.meta_main_portrait.genes).map(([key, values]) => (
							<tr key={key}>
								<th>{convertToReadable(key)}</th>
								<td>{values.join(" | ")}</td>
							</tr>
						))}
					</tbody>
				</table>
			</div>
		</div>
	);
}
