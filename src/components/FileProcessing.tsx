import React from "react";

export function FileProcessing(): JSX.Element {
	return (
		<div>
			<div>Processing file...</div>
			<progress />
		</div>
	);
}
