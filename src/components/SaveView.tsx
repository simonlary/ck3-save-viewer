import React from "react";
import { Save } from "../parser/parser";
import { MetadataView } from "./Metadata/MetadataView";
import { ReligionView } from "./Religion/ReligionView";

interface SaveViewProps {
	save: Save;
}

export function SaveView(props: SaveViewProps): JSX.Element {
	return <ReligionView religions={props.save.religion} />;
	// return <MetadataView metadata={props.save.meta_data} />;
}
