import { SaveFileReligion } from "@simonlary/ck3-save-parser/dist/save";
import React from "react";

interface ReligionViewProps {
	religions: SaveFileReligion;
}

export function ReligionView(props: ReligionViewProps): JSX.Element {
	console.log(props.religions);
	return (
		<ul>
			{Object.entries(props.religions.religions).map(([religionIndex, religion]) => (
				<li key={religionIndex}>
					{religion.tag}
					<ul>
						{religion.faiths.map((faithIndex) => (
							<li key={faithIndex}>{props.religions.faiths[faithIndex].tag}</li>
						))}
					</ul>
				</li>
			))}
		</ul>
	);
}
