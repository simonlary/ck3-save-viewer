import React from "react";

interface FileUploadProps {
	onFileUploaded: (file: File) => void;
}

export function FileUpload(props: FileUploadProps): JSX.Element {
	const selectFile = (event: React.ChangeEvent<HTMLInputElement>) => {
		const { files } = event.target;
		if (files === null || files.length < 1) return;
		const file = files[0];
		props.onFileUploaded(file);
	};

	return (
		<div>
			<input type="file" onChange={selectFile} />
		</div>
	);
}
