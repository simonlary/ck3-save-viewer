import React, { useEffect, useState } from "react";
import { parseContent, parseFile, Save } from "../parser/parser";
import { FileProcessing } from "./FileProcessing";
import { FileUpload } from "./FileUpload";
import { SaveView } from "./SaveView";

interface WaitingState {
	status: "waiting";
}

interface ProcessingState {
	status: "processing";
	file: File;
}

interface ParsedState {
	status: "parsed";
	save: Save;
}

type State = WaitingState | ProcessingState | ParsedState;

async function parse(file: File): Promise<Save> {
	return file.name === "gamestate" ? await parseContent(await file.text()) : await parseFile(file);
}

export function App(): JSX.Element {
	const [state, setState] = useState<State>({ status: "waiting" });

	useEffect(() => {
		if (state.status !== "processing") return;

		let cancelled = false;
		parse(state.file).then((save) => {
			if (!cancelled) setState({ status: "parsed", save });
		});
		return () => {
			cancelled = true;
		};
	}, [state]);

	switch (state.status) {
		case "waiting":
			return <FileUpload onFileUploaded={(file) => setState({ status: "processing", file })} />;
		case "processing":
			return <FileProcessing />;
		case "parsed":
			return <SaveView save={state.save} />;
	}
}
