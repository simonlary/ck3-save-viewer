import { Save } from "@simonlary/ck3-save-parser";

export type MainToWorkerMessage = { type: "parseFile"; data: File } | { type: "parseContent"; data: string };

export type WorkerToMainMessage = { type: "result"; data: Save };
