import { parseContent, parseFile } from "@simonlary/ck3-save-parser";
import { MainToWorkerMessage, WorkerToMainMessage } from "./parser-worker-message";

const ctx = (self as unknown) as Worker;

ctx.addEventListener("message", async (event: MessageEvent) => {
	const message = event.data as MainToWorkerMessage;
	const result = await main(message);
	ctx.postMessage(result);
});

async function main(message: MainToWorkerMessage): Promise<WorkerToMainMessage> {
	switch (message.type) {
		case "parseContent":
			return {
				type: "result",
				data: parseContent(message.data),
			};
		case "parseFile":
			return {
				type: "result",
				data: await parseFile(message.data),
			};
	}
}
