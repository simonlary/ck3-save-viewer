import { Save } from "@simonlary/ck3-save-parser";
import { MainToWorkerMessage, WorkerToMainMessage } from "./parser-worker-message";

const worker = new Worker(new URL("./parser.worker.ts", import.meta.url));

async function postMessage(message: MainToWorkerMessage): Promise<WorkerToMainMessage> {
	worker.postMessage(message);
	return new Promise((resolve) => {
		worker.addEventListener(
			"message",
			(event: MessageEvent) => {
				resolve(event.data as WorkerToMainMessage);
			},
			{ once: true }
		);
	});
}

export async function parseFile(file: File): Promise<Save> {
	const result = await postMessage({
		type: "parseFile",
		data: file,
	});
	return result.data;
}

export async function parseContent(content: string): Promise<Save> {
	const result = await postMessage({
		type: "parseContent",
		data: content,
	});
	return result.data;
}

export { Save } from "@simonlary/ck3-save-parser";
